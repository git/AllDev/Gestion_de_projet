<div align = center>

  <img src="Documentation/Images/Banner-AllIn-Dark.png" />
    
---

[Présentation](#gestion-de-projet---all-in) | [Rapports](#rapports) | [Sprints](#sprints) | [Diagrammes](#diagrammes) | [Outils](#outils) | [Wiki](https://codefirst.iut.uca.fr/git/AllDev/Gestion_de_projet/wiki)

---

</div>

### Gestion de projet - ALL IN !

**Description** : Ce dépôt regroupe l'ensemble des éléments de gestion de projet mis en place par les membres de l'équipe *ALL IN*, permettant de centraliser les informations, les documents et d'établir des méthodes d'organisation et de suivi de projets.
</br>

Lien vers le wiki pour une vision plus complète du projet : [Wiki ALL IN](https://codefirst.iut.uca.fr/git/AllDev/Gestion_de_projet/wiki) !

# Rapports

- [Rapport de planification de projet](Documentation/Rapports/Gestion%20de%20projet%20-%20ALL%20IN.pdf)

# Sprints

Durant ce projet, nous avons opté pour l'application des méthodes Scrum Light, mettant en valeur leur flexibilité et leur adaptabilité. Chacun de nos sprints, d'une durée générale de deux semaines, se concentre sur des objectifs spécifiques. À la fin de chaque sprint, nous réalisons une revue des tâches accomplies, assurant ainsi une traçabilité et une évaluation continues de notre progression.

Lien vers les sprints:

   - [Sprint 0](https://codefirst.iut.uca.fr/git/AllDev/Gestion_de_projet/wiki/Sprint-0)
   - [Sprint 1](https://codefirst.iut.uca.fr/git/AllDev/Gestion_de_projet/wiki/Sprint-1)
   - [Sprint 2](https://codefirst.iut.uca.fr/git/AllDev/Gestion_de_projet/wiki/Sprint-2)
   - [Sprint 3](https://codefirst.iut.uca.fr/git/AllDev/Gestion_de_projet/wiki/Sprint-3)
   - [Sprint 4](https://codefirst.iut.uca.fr/git/AllDev/Gestion_de_projet/wiki/Sprint-4)
   - [Sprint 5](https://codefirst.iut.uca.fr/git/AllDev/Gestion_de_projet/wiki/Sprint-5)
   - [Sprint 6](https://codefirst.iut.uca.fr/git/AllDev/Gestion_de_projet/wiki/Sprint-6)
   - [Sprint 7](https://codefirst.iut.uca.fr/git/AllDev/Gestion_de_projet/wiki/Sprint-7)

# Diagrammes

### MCD et MLD

...

### Diagramme de classes

![Diagramme de classes](Documentation/Diagrammes/AllInModel.png)

# Outils

### [Kanban 📌](https://codefirst.iut.uca.fr/git/AllDev/Gestion_de_projet/projects/379)

Le Kanban que nous utilisons grâce à Code First nous permet de visualiser les différentes tâches à réaliser tout au long du projet, ainsi que leur progression. Il contribue efficacement au suivi de l’avancement des tâches, à la gestion des priorités et à la facilitation de la communication entre les membres de l'équipe. Cet outil se révèle particulièrement utile pour analyser la qualité de notre travail, offrant une vue d'ensemble depuis la planification du projet jusqu'à la livraison finale, en passant par la réalisation et la validation.
</br>

### [Backlog 📊](https://codefirst.iut.uca.fr/git/AllDev/Gestion_de_projet/wiki/Backlog-Product)

Le Backlog, joue un rôle crucial dans notre gestion de projet. Nous estimons minutieusement chaque tâche en termes de durée et d'importance, permettant ainsi une planification précise et une meilleure compréhension des priorités. Cette approche nous aide à maintenir une traçabilité efficace tout en assurant une allocation optimale des ressources.

### Discord 💻

La communication entre les membres de l'équipe, que ce soit pour se tenir informés ou solliciter des informations, s'effectue via Discord en raison de la familiarité de chacun avec cette plateforme.

<div align = center>

© AllDev - Gestion

</div>